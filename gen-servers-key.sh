#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh

dir=$1

if [ ! -d $dir ]; then
    echo "$dir not a directory"
    exit 1
fi


gen_key() {
    if [ $# -ne 2 ]; then
        echo "usage: ${FUNCNAME[0]} server_dir sign_dir"
        return 1
    fi
    local server_dir=$1
    local sign_dir=$2
    if [ -d $server_dir/pki ]; then
        return 0;
    fi
    local instance=$__DIR__/instance
    mkdir -p $instance/{etc/salt,var/cache/salt,var/run,var/log}
    cat <<EOF >$instance/master
root_dir: $instance/
EOF
    salt-master -c $instance &>$instance/salt.log &
    local pid=$!
    local pki_dir=$instance/etc/salt/pki
    local master_pki_dir=$pki_dir/master
    while [ ! -r $master_pki_dir/master.pem ]; do
        sleep 0.5
    done
    if [ -r $sign_dir/master_sign.pem ]; then
        cp $sign_dir/master_sign.pem $master_pki_dir
        cp $sign_dir/master_sign.pub $master_pki_dir
    fi
    salt-key -c $instance --auto-create --gen-signature
    if [ ! -r $sign_dir/master_sign.pem ]; then
        cp $master_pki_dir/master_sign.pem $sign_dir
        cp $master_pki_dir/master_sign.pub $sign_dir
    fi
    rm -f $master_pki_dir/master_sign.pem
    kill $pid
    while ! grep -q -m 1 Exited $instance/salt.log; do
        sleep 0.5
    done
    mv $pki_dir $server_dir
}


for l in $(_list_d $dir/server); do
    while read -u 3 -r line; do
        read_into_vars_assert server <<<"$line"

        LOG server $server
        mkdir -p $dir/{server/$server,sign}
        gen_key $dir/server/$server $dir/sign
        LOG server $server finished
    done 3< <(grep -v '^\s*#' $l)
done
