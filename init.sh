#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh

dir=$1
mkdir -p $dir/server
mkdir -p $dir/client
mkdir -p $dir/sign

touch $dir/{client,server}.list
