#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh

dir=$1

if [ ! -d $dir ]; then
    echo "$dir not a directory"
    exit 1
fi

cd $dir/client
for l in $(_list_d $dir/client); do
    while read -u 3 -r line; do
        read_into_vars_assert client <<<"$line"
        if [ ! -r $client.pem ]; then
            salt-key --gen-keys=$client
        fi
    done 3< <(grep -v '^\s*#' $l)
done

for l in $(_list_d $dir/server); do
    while read -u 3 -r line; do
        read_into_vars_assert server <<<"$line"
        minions_dir=$dir/server/$server/pki/master/minions
        mkdir -p $minions_dir
        for pub in *.pub; do
            cp $pub $minions_dir/${pub%.pub}
        done
    done 3< <(grep -v '^\s*#' $l)
done
cd - >/dev/null
